/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

    Definiciones globales del sistema.
*/

#ifndef __DEFINES_H__
#define __DEFINES_H__

/* Bool */
/* -------------------------------------------------------------------------- */
#define TRUE                    0x00000001
#define FALSE                   0x00000000
#define ERROR                   1

/* Atributos paginas */
/* -------------------------------------------------------------------------- */
#define PAG_P                   0x00000001
#define PAG_R                   0x00000000
#define PAG_RW                  0x00000002
#define PAG_S                   0x00000000
#define PAG_US                  0x00000004

/* Misc */
/* -------------------------------------------------------------------------- */
#define SIZE_N                  40 // X
#define SIZE_M                  78 // Y

/* Indices en la gdt */
/* -------------------------------------------------------------------------- */
#define GDT_COUNT 45

#define GDT_IDX_NULL_DESC           0
#define GDT_IDX_DATA_0_DESC         18
#define GDT_IDX_CODE_0_DESC         19
#define GDT_IDX_DATA_3_DESC         20
#define GDT_IDX_CODE_3_DESC         21
#define GDT_IDX_VIDEO_DESC          22

#define GDT_IDX_TSS_INICIAL         23
#define GDT_IDX_TSS_IDLE            24

#define GDT_IDX_TSS_TAREA_1_A       25
#define GDT_IDX_TSS_TAREA_2_A       26
#define GDT_IDX_TSS_TAREA_3_A       27
#define GDT_IDX_TSS_TAREA_4_A       28
#define GDT_IDX_TSS_TAREA_5_A       29
#define GDT_IDX_TSS_TAREA_6_A       30
#define GDT_IDX_TSS_TAREA_7_A       31
#define GDT_IDX_TSS_TAREA_8_A       32
#define GDT_IDX_TSS_TAREA_9_A       33
#define GDT_IDX_TSS_TAREA_10_A      34

#define GDT_IDX_TSS_TAREA_1_B       35
#define GDT_IDX_TSS_TAREA_2_B       36
#define GDT_IDX_TSS_TAREA_3_B       37
#define GDT_IDX_TSS_TAREA_4_B       38
#define GDT_IDX_TSS_TAREA_5_B       39
#define GDT_IDX_TSS_TAREA_6_B       40
#define GDT_IDX_TSS_TAREA_7_B       41
#define GDT_IDX_TSS_TAREA_8_B       42
#define GDT_IDX_TSS_TAREA_9_B       43
#define GDT_IDX_TSS_TAREA_10_B      44


/* Offsets en la gdt */
/* -------------------------------------------------------------------------- */
#define GDT_OFF_NULL_DESC           (GDT_IDX_NULL_DESC << 3)
#define GDT_OFF_DATA_0_DESC         (GDT_IDX_DATA_0_DESC << 3)
#define GDT_OFF_CODE_0_DESC         (GDT_IDX_CODE_0_DESC << 3)
#define GDT_OFF_DATA_3_DESC         (GDT_IDX_DATA_3_DESC << 3)
#define GDT_OFF_CODE_3_DESC         (GDT_IDX_CODE_3_DESC << 3)
#define GDT_OFF_VIDEO_DESC          (GDT_IDX_VIDEO_DESC << 3)

/* Selectores de segmentos */
/* -------------------------------------------------------------------------- */

/* Direcciones de memoria */
/* -------------------------------------------------------------------------- */
#define BOOTSECTOR               0x00001000 /* direccion fisica de comienzo del bootsector (copiado) */
#define KERNEL                   0x00001200 /* direccion fisica de comienzo del kernel */
#define VIDEO                    0x000B8000 /* direccion fisica del buffer de video */

/* Direcciones virtuales de código, pila y datos */
/* -------------------------------------------------------------------------- */
#define TASK_CODE             0x08000000 /* direccion virtual del codigo */

/* Direcciones fisicas de codigos */
/* -------------------------------------------------------------------------- */
/* En estas direcciones estan los códigos de todas las tareas. De aqui se
 * copiaran al destino indicado por TASK_<i>_CODE_ADDR.
 */

/* Direcciones fisicas de directorios y tablas de paginas del KERNEL */
/* -------------------------------------------------------------------------- */
#define KERNEL_PAGE_DIR          0x00027000
#define KERNEL_PAGE_TABLE_0      0x00028000
#define CRYSTALS_MAP             0x00029000

#endif  /* !__DEFINES_H__ */
