/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
*/

#ifndef __GAME_H__
#define __GAME_H__

#include "stdint.h"
#include "defines.h"
#include "screen.h"
#include "mmu.h"
#include "sched.h"

typedef enum e_direction {
    Right = 1,
    Left = 2,
    Forward = 3,
    Back = 4,
} e_direction_t;

void game_init();

typedef struct task_t {
	int posx;
	int posy;
	int cristales; //max 30
} __attribute__((__packed__, aligned (8))) task_desc;

void tasks_init();

task_desc tasks[20]; /*
    									0 a 9: 	 tareas de A
    									10 a 19: tareas de B
    								*/

typedef struct player_t {
	uint16_t posy;
  uint32_t score;
} __attribute__((__packed__, aligned (8))) player;

player players[2];/*
	                   0: 	player A
    								 1:  player B
    							*/

uint8_t map[80][40];

void screen_init();

void map_init();

void is_finished();

#endif  /* !__GAME_H__ */
