/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del scheduler
*/

#include "sched.h"


void sched_init(){
	//Inicializo los vectores de estados
	for (int i=0; i<10; i++){
		scheduler->estadosA[i] = 0;
		scheduler->estadosB[i] = 0;
	}
	//Inicializo las tareas actuales de cada jugador en un número mayor al de la cantidad de tareas
	scheduler->indiceTareaActualA = 11;
	scheduler->indiceTareaActualB = 11;
	//Inicializo el jugador actual en 2 para indicar que el juego no comenzó y está corriendo la idle
	scheduler->jugadorActual = 2;
}

uint16_t sched_nextTask(){
	if(	scheduler->jugadorActual == 2){
		scheduler->jugadorActual = 1;
	} else {
		scheduler->jugadorActual = 	neg(scheduler->jugadorActual);
	}
	return player_nextTask(scheduler->jugadorActual);
}

uint16_t player_nextTask(uint8_t player){
	uint8_t* vector = (player ? (scheduler->estadosA) : (scheduler->estadosB));
	uint8_t next = (player ? (scheduler->indiceTareaActualA) : (scheduler->indiceTareaActualB));
	uint8_t found = 0;
	for (uint8_t i = 0; i < 10; i++){
		next = (next +1)%10;
		if(vector[next]){
			i = 10;
			found = 1;
		}
	}
	if(!found){
		return 24<<3; //Idle
	} else {
		if(player){
			scheduler->indiceTareaActualA = next;
			return ((next+25)<<3);
		} else {
			scheduler->indiceTareaActualB = next;
			return ((next+35)<<3);
		}
	}
}

uint8_t neg(uint8_t player){
	return (player ? 0 : 1);
}

uint8_t current_id(){
	return (scheduler->jugadorActual ? (scheduler->indiceTareaActualA +1) : (scheduler->indiceTareaActualB +1));
}

uint16_t kill_task(){
	if(scheduler->jugadorActual){
		scheduler->estadosA[scheduler->indiceTareaActualA] = 0;
		print((uint8_t *) "x\0", 3*(scheduler->indiceTareaActualA+1)+1, 44, C_FG_RED | C_BG_BLACK);
		print_dec(map[tasks[scheduler->indiceTareaActualA].posx][tasks[scheduler->indiceTareaActualA].posy], 1, tasks[scheduler->indiceTareaActualA].posx, tasks[scheduler->indiceTareaActualA].posy, C_FG_BLUE | C_BG_CYAN);
	} else {
		scheduler->estadosB[scheduler->indiceTareaActualB] = 0;
		print((uint8_t *) "x\0", 3*(scheduler->indiceTareaActualB+1)+1+46, 44, C_FG_RED | C_BG_BLACK);
		print_dec(map[tasks[scheduler->indiceTareaActualB+10].posx][tasks[scheduler->indiceTareaActualB+10].posy], 1, tasks[scheduler->indiceTareaActualB+10].posx, tasks[scheduler->indiceTareaActualB+10].posy, C_FG_BLUE | C_BG_CYAN);
	}
	return player_nextTask(scheduler->jugadorActual);
}

uint8_t get_free_pos(uint8_t player){
	uint8_t free_pos = 0;
	uint8_t found = 0;
	uint8_t* vector = (player ? (scheduler->estadosA) : (scheduler->estadosB));
	while(!found && free_pos < 10){
		if(!vector[free_pos]){
			found = 1;
		} else {
			free_pos++;
		}
	}
	return (found ? free_pos : 11);
}
