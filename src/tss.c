/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de estructuras para administrar tareas
*/

#include "tss.h"



void tss_init() {
  gdt_tss_inicializar();
  init_tss_idle();
  for (uint8_t i = 0; i < 10; i++) {
    tss_a[i] = init_tss_tarea(1, i+25, i);
    gdt[i+25].base_0_15 = (uint16_t) ( (uint32_t) &tss_a[i]);
    gdt[i+25].base_23_16 = (uint16_t) ( (uint32_t) &tss_a[i] >> 16);
    gdt[i+25].base_31_24= (uint16_t) ( (uint32_t) &tss_a[i] >> 24);
    tss_b[i] = init_tss_tarea(0, i+35, i);
    gdt[i+35].base_0_15 = (uint16_t) ( (uint32_t) &tss_b[i]);
    gdt[i+35].base_23_16 = (uint16_t) ( (uint32_t) &tss_b[i] >> 16);
    gdt[i+35].base_31_24= (uint16_t) ( (uint32_t) &tss_b[i] >> 24);
  }
}

void gdt_tss_inicializar(){
  gdt[GDT_IDX_TSS_IDLE].base_0_15 = (uint16_t) ( ( (uint32_t) &tss_idle));
  gdt[GDT_IDX_TSS_IDLE].base_23_16 = (uint16_t) ( ( (uint32_t) &tss_idle) >> 16);
  gdt[GDT_IDX_TSS_IDLE].base_31_24= (uint16_t) ( ( (uint32_t) &tss_idle) >> 24);

  gdt[GDT_IDX_TSS_INICIAL].base_0_15 = (uint16_t) ( ( (uint32_t) &tss_initial));
  gdt[GDT_IDX_TSS_INICIAL].base_23_16 = (uint16_t) ( ( (uint32_t) &tss_initial) >> 16);
  gdt[GDT_IDX_TSS_INICIAL].base_31_24= (uint16_t) ( ( (uint32_t) &tss_initial) >> 24);
}

void init_tss_idle(){
  tss_idle.ptl = 0;
  tss_idle.esp0 = 0x27000; //pila kernel
  tss_idle.ss0 = (GDT_IDX_DATA_0_DESC << 3);
  tss_idle.cr3 = 0x27000; //cr3 kernel
  tss_idle.eip = 0x14000;
  tss_idle.eflags = 0x00000202; //interrupciones;
  tss_idle.eax = 0;
  tss_idle.ecx = 0;
  tss_idle.edx = 0;
  tss_idle.ebx = 0;
  tss_idle.esp = 0x27000;
  tss_idle.ebp = 0x27000;
  tss_idle.esi = 0;
  tss_idle.edi = 0;
  tss_idle.es = (GDT_IDX_DATA_0_DESC << 3);
  tss_idle.cs = (GDT_IDX_CODE_0_DESC << 3);
  tss_idle.ss = (GDT_IDX_DATA_0_DESC << 3);
  tss_idle.ds = (GDT_IDX_DATA_0_DESC << 3);
  tss_idle.fs = (GDT_IDX_DATA_0_DESC << 3);
  tss_idle.gs = (GDT_IDX_DATA_0_DESC << 3);
  tss_idle.iomap = 0xFFFF;

  tss_idle.unused10 = 0x0;
  tss_idle.unused9 = 0x0;
  tss_idle.unused8 = 0x0;
  tss_idle.unused7 = 0x0;
  tss_idle.unused6 = 0x0;
  tss_idle.unused5 = 0x0;
  tss_idle.unused4 = 0x0;
  tss_idle.unused3 = 0x0;
  tss_idle.unused2 = 0x0;
  tss_idle.unused1 = 0x0;
  tss_idle.unused0 = 0x0;
  tss_idle.dtrap = 0x0;
  tss_idle.edi = 0x0;
  tss_idle.esi = 0x0;
  tss_idle.ebx = 0x0;
  tss_idle.edx = 0x0;
  tss_idle.ecx = 0x0;
  tss_idle.eax = 0x0;
  tss_idle.ss2 = 0x0;
  tss_idle.esp2 = 0x0;
  tss_idle.ss1 = 0x0;
  tss_idle.esp1 = 0x0;
  tss_idle.ptl = 0x0;
  tss_idle.ldt = 0x0;
}

tss init_tss_tarea(uint8_t player, uint16_t idx, uint32_t posy){
  uint32_t posx = (player ? 1 : 78);
  uint8_t id = idx-25;

  tss tss_nueva_tarea;

  tss_nueva_tarea.eip = 0x08000000;
  tss_nueva_tarea.esp = 0x08002000;
  tss_nueva_tarea.ebp = 0x08002000;
  tss_nueva_tarea.eflags = 0x00000202;
  tss_nueva_tarea.iomap = 0xffff;
  tss_nueva_tarea.cs = (GDT_IDX_CODE_3_DESC << 3) | 0x3; //rpl en 11
  tss_nueva_tarea.ss = (GDT_IDX_DATA_3_DESC << 3) | 0x3; //rpl en 11
  tss_nueva_tarea.gs = (GDT_IDX_DATA_3_DESC << 3) | 0x3; //rpl en 11
  tss_nueva_tarea.ds = (GDT_IDX_DATA_3_DESC << 3) | 0x3; //rpl en 11
  tss_nueva_tarea.fs = (GDT_IDX_DATA_3_DESC << 3) | 0x3; //rpl en 11
  tss_nueva_tarea.es = (GDT_IDX_DATA_3_DESC << 3) | 0x3; //rpl en 11
  tss_nueva_tarea.cr3 = (uint32_t) mmu_initTaskDir(player, id, posx, posy);; //cr3 tarea
  tss_nueva_tarea.esp0 = mmu_nextFreeKernelPage() + 0x1000; //pila nivel 0
  tss_nueva_tarea.ss0 = GDT_IDX_DATA_0_DESC << 3; //rpl en 00

  tss_nueva_tarea.unused10 = 0x0;
  tss_nueva_tarea.unused9 = 0x0;
  tss_nueva_tarea.unused8 = 0x0;
  tss_nueva_tarea.unused7 = 0x0;
  tss_nueva_tarea.unused6 = 0x0;
  tss_nueva_tarea.unused5 = 0x0;
  tss_nueva_tarea.unused4 = 0x0;
  tss_nueva_tarea.unused3 = 0x0;
  tss_nueva_tarea.unused2 = 0x0;
  tss_nueva_tarea.unused1 = 0x0;
  tss_nueva_tarea.unused0 = 0x0;
  tss_nueva_tarea.dtrap = 0x0;
  tss_nueva_tarea.edi = 0x0;
  tss_nueva_tarea.esi = 0x0;
  tss_nueva_tarea.ebx = 0x0;
  tss_nueva_tarea.edx = 0x0;
  tss_nueva_tarea.ecx = 0x0;
  tss_nueva_tarea.eax = 0x0;
  tss_nueva_tarea.ss2 = 0x0;
  tss_nueva_tarea.esp2 = 0x0;
  tss_nueva_tarea.ss1 = 0x0;
  tss_nueva_tarea.esp1 = 0x0;
  tss_nueva_tarea.ptl = 0x0;
  tss_nueva_tarea.ldt = 0x0;

  return tss_nueva_tarea;
}
