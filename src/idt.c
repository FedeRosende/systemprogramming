/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de las rutinas de atencion de interrupciones
*/

#include "defines.h"
#include "idt.h"
#include "isr.h"
#include "tss.h"
#include "game.h"
#include "mmu.h"
#include "i386.h"


idt_entry idt[255] = { };

idt_descriptor IDT_DESC = {
    sizeof(idt) - 1,
    (uint32_t) &idt
};

#define IDT_ENTRY(numero)                                                                          \
    idt[numero].offset_0_15 = (uint16_t) ((uint32_t)(&_isr ## numero) & (uint32_t) 0xFFFF);        \
    idt[numero].segsel = (uint16_t) GDT_OFF_CODE_0_DESC;                                           \
    idt[numero].attr = (uint16_t) 0x8E00;                                                          \
    idt[numero].offset_16_31 = (uint16_t) ((uint32_t)(&_isr ## numero) >> 16 & (uint32_t) 0xFFFF);

#define IDT_ENTRY_USR(numero)                                                                          \
        idt[numero].offset_0_15 = (uint16_t) ((uint32_t)(&_isr ## numero) & (uint32_t) 0xFFFF);        \
        idt[numero].segsel = (uint16_t) GDT_OFF_CODE_0_DESC;                                           \
        idt[numero].attr = (uint16_t) 0xEE00;                                                          \
        idt[numero].offset_16_31 = (uint16_t) ((uint32_t)(&_isr ## numero) >> 16 & (uint32_t) 0xFFFF);

void idt_init() {
    // Excepciones
    IDT_ENTRY(0);
    IDT_ENTRY(1);
    IDT_ENTRY(2);
    IDT_ENTRY(3);
    IDT_ENTRY(4);
    IDT_ENTRY(5);
    IDT_ENTRY(6);
    IDT_ENTRY(7);
    IDT_ENTRY(8);
    IDT_ENTRY(9);
    IDT_ENTRY(10);
    IDT_ENTRY(11);
    IDT_ENTRY(12);
    IDT_ENTRY(13);
    IDT_ENTRY(14);
    IDT_ENTRY(15);
    IDT_ENTRY(16);
    IDT_ENTRY(17);
    IDT_ENTRY(18);
    IDT_ENTRY(19);
    //Reloj y teclado
    IDT_ENTRY(32);
    IDT_ENTRY(33);
    //Interrupción 47
    IDT_ENTRY_USR(47);
}

void move_task(e_direction_t dir){
  uint8_t task_idx = ((scheduler->jugadorActual) ? (scheduler->indiceTareaActualA) : (scheduler->indiceTareaActualB + 10));
  uint8_t color = ((scheduler->jugadorActual) ? 0x0A : 0X0E);
  uint8_t oldx = tasks[task_idx].posx;
  uint8_t oldy = tasks[task_idx].posy;

  if(dir == Right){
    if(task_idx < 10){ //si es tarea de A
      tasks[task_idx].posy = (((tasks[task_idx].posy+1) == 41) ? 0 : (tasks[task_idx].posy+1));
    } else { //si es tarea de B
      tasks[task_idx].posy = (((tasks[task_idx].posy-1) == -1) ? 40 : (tasks[task_idx].posy-1));
    }
  } else if (dir == Left){
    if(task_idx > 9){ //si es tarea de B
      tasks[task_idx].posy = (((tasks[task_idx].posy+1) == 41) ? 0 : (tasks[task_idx].posy+1));
    } else { //si es tarea de A
      tasks[task_idx].posy = (((tasks[task_idx].posy-1) == -1) ? 40 : (tasks[task_idx].posy-1));
    }
  } else if (dir == Forward){
    if(task_idx < 10){ //si es tarea de A
      if( (tasks[task_idx].posx + 1) == 78){
        players[1].score += tasks[task_idx].cristales;
        tasks[task_idx].cristales = 0;
        tasks[task_idx].posx = 78;
      } else {
        tasks[task_idx].posx = (tasks[task_idx].posx + 1);
      }
    } else { //si es tarea de B
      if( (tasks[task_idx].posx - 1) == 1){
        players[0].score += tasks[task_idx].cristales;
        tasks[task_idx].cristales = 0;
        tasks[task_idx].posx = 1;
      } else {
        tasks[task_idx].posx = (tasks[task_idx].posx - 1);
      }
    }
  } else if (dir == Back) {
    if(task_idx > 9){ //si es tarea de B
      if( (tasks[task_idx].posx + 1) == 78){
        players[1].score += tasks[task_idx].cristales;
        print_dec(players[1].score, 3, 41, 44, C_FG_WHITE | C_BG_BLUE);
        tasks[task_idx].cristales = 0;
        print_dec(tasks[task_idx].cristales, 2, (task_idx*3)+19, 45, C_FG_LIGHT_GREY | C_BG_BLACK);
        tasks[task_idx].posx = 78;
      } else {
        tasks[task_idx].posx = (tasks[task_idx].posx + 1);
      }
    } else { //si es tarea de A
      if( (tasks[task_idx].posx - 1) == 1){
        players[0].score += tasks[task_idx].cristales;
        print_dec(players[0].score, 3, 36, 44, C_FG_WHITE | C_BG_RED);
        tasks[task_idx].cristales = 0;
        print_dec(tasks[task_idx].cristales, 2, (task_idx*3)+3, 45, C_FG_LIGHT_GREY | C_BG_BLACK);
        tasks[task_idx].posx = 1;
      } else {
        tasks[task_idx].posx = (tasks[task_idx].posx - 1);
      }
    }
  }

  uint32_t new_pos = game_calcularPos(tasks[task_idx].posx, tasks[task_idx].posy);
  mmu_refreshPos(scheduler->jugadorActual, cr3s[task_idx], new_pos);
  print_dec(map[oldx][oldy], 1, oldx, oldy, C_FG_BLUE | C_BG_CYAN);
  print_dec(task_idx, 1, tasks[task_idx].posx, tasks[task_idx].posy, color);
}

int task_take(){
  uint8_t task_idx = ((scheduler->jugadorActual) ? (scheduler->indiceTareaActualA) : (scheduler->indiceTareaActualB + 10));
  int cristales_a_agarrar = map[tasks[task_idx].posx][tasks[task_idx].posy];
  if(cristales_a_agarrar != 0){
    if(tasks[task_idx].cristales + cristales_a_agarrar < 30){
      tasks[task_idx].cristales += cristales_a_agarrar;
      map[tasks[task_idx].posx][tasks[task_idx].posy] = 0;
      if(task_idx > 9){
        print_dec(tasks[task_idx].cristales, 2, (task_idx*3)+19, 45, C_FG_LIGHT_GREY | C_BG_BLACK);
      } else {
        print_dec(tasks[task_idx].cristales, 2, (task_idx*3)+3, 45, C_FG_LIGHT_GREY | C_BG_BLACK);
      }
      return cristales_a_agarrar;
    } else {
      return 0;
    }
  }
  return -1;
}

void crear_tarea_A(){
  uint8_t task_idx = get_free_pos(1);
  if (task_idx != 11){
    tss_a[task_idx] = init_tss_tarea(1, task_idx+25, task_idx);
    mmu_mapNewPos(1, cr3s[task_idx], 1, players[0].posy);
    tasks[task_idx].posx = 1;
    tasks[task_idx].posy = players[0].posy;
    scheduler->estadosA[task_idx] = 1;
    print_dec(task_idx, 1, tasks[task_idx].posx, tasks[task_idx].posy, 0x0A);
  }
}

void crear_tarea_B(){
  uint8_t task_idx = get_free_pos(0);
  if (task_idx != 11){
    tss_b[task_idx] = init_tss_tarea(0, task_idx+35, task_idx);

    mmu_mapNewPos(0, cr3s[task_idx+10], 78, players[1].posy);
    tasks[task_idx+10].posx = 78;
    tasks[task_idx+10].posy = players[1].posy;
    scheduler->estadosB[task_idx] = 1;
    print_dec(task_idx+10, 1, tasks[task_idx+10].posx, tasks[task_idx+10].posy, 0x0E);
  }
}

void bajo_A(){
  if(players[0].posy+1 < 41){
    print_dec(0, 1, 0, players[0].posy, 0x00);
    players[0].posy += 1;
    print_dec(0, 1, 0, players[0].posy, 0x04);
  }
}

void subo_A(){
  if(players[0].posy-1 > -1){
    print_dec(0, 1, 0, players[0].posy, 0x00);
    players[0].posy -= 1;
    print_dec(0, 1, 0, players[0].posy, 0x04);
  }
}


void bajo_B(){
  if(players[1].posy+1 < 41){
    print_dec(0, 1, 79, players[1].posy, 0x00);
    players[1].posy += 1;
    print_dec(0, 1, 79, players[1].posy, 0x01);
  }
}

void subo_B(){
  if(players[1].posy-1 > -1){
    print_dec(0, 1, 79, players[1].posy, 0x00);
    players[1].posy -= 1;
    print_dec(0, 1, 79, players[1].posy, 0x01);
  }
}

void print_debug_mode(){
  char* debug = "MODO DEBUG\0";
  print((uint8_t*)debug, 35, 49, C_FG_WHITE | C_BG_MAGENTA);
}

void print_normal_mode(){
  screen_drawBox(49, 35, 1, 10, 0, 0x00);
}

void print_procesador(uint32_t esp){
  screen_drawBox(1, 20, 39, 40, 0, C_BG_LIGHT_GREY);
  //screen_drawBox(2, 21, 38, 39, 0, 0xFF);

  print((uint8_t*)"eax\0", 22, 5, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(*(uint32_t *)(esp+28), 8, 29, 5, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"ebx\0", 22, 7, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(*(uint32_t *)(esp+16), 8, 29, 7, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"ecx\0", 22, 9, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(*(uint32_t *)(esp+24), 8, 29, 9, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"edx\0", 22, 11, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(*(uint32_t *)(esp+20), 8, 29, 11, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"esi\0", 22, 13, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(*(uint32_t *)(esp+4), 8, 29, 13, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"edi\0", 22, 15, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(*(uint32_t *)(esp+0), 8, 29, 15, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"ebp\0", 22, 17, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(*(uint32_t *)(esp+8), 8, 29, 17, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"esp\0", 22, 19, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(*(uint32_t *)(esp+12), 8, 29, 19, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"eip\0", 22, 21, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(*(uint32_t *)(esp+32), 8, 29, 21, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"cs\0", 22, 23, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(rcs(), 4, 29, 23, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"ds\0", 22, 25, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(rds(), 4, 29, 25, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"es\0", 22, 27, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(rds(), 4, 29, 27, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"fs\0", 22, 29, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(rds(), 4, 29, 29, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"gs\0", 22, 31, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(rds(), 4, 29, 31, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"ss\0", 22, 33, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(rss(), 4, 29, 33, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"eflags\0", 22, 35, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(*(uint32_t *)(esp+40), 8, 29, 35, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"cr0\0", 46, 5, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(rcr0(), 8, 50, 5, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"cr2\0", 46, 7, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(rcr2(), 8, 50, 7, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"cr3\0", 46, 9, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(rcr3(), 8, 50, 9, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"cr4\0", 46, 11, C_FG_BLACK | C_BG_LIGHT_GREY);
  print_hex(rcr4(), 8, 50, 11, C_FG_WHITE | C_BG_LIGHT_GREY);

  print((uint8_t*)"stack\0", 46, 15, C_FG_BLACK | C_BG_LIGHT_GREY);
  uint32_t stack = *(uint32_t *)(esp+12);
  uint8_t i = 1;
  while (stack<=0x08000ffc && i < 6) {
    print_hex(*(uint32_t*)(stack), 8, 46, 15+2*i, C_FG_WHITE | C_BG_LIGHT_GREY);
    stack+= 4;
    i++;
  }
}

void print_map_task(){
  for (uint16_t i = 1; i < 79; i++) {
	  for (uint16_t j = 1; j < 40; j++) {
			print_dec(map[i][j], 1, i, j, C_FG_BLUE | C_BG_CYAN);
	  }
	}
  for (uint16_t i = 0; i < 10; i++) {
    if(scheduler->estadosA[i] == 1){
      print_dec(i, 1, tasks[i].posx, tasks[i].posy, 0x0A);
    }
    if(scheduler->estadosB[i] == 1){
      print_dec(i, 1, tasks[i+10].posx, tasks[i+10].posy, 0x0E);
    }
  }
}
