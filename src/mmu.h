/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del manejador de memoria
*/

#ifndef __MMU_H__
#define __MMU_H__

#include "stdint.h"
#include "defines.h"
#include "i386.h"
#include "tss.h"
#include "game.h"


void mmu_init();

uint32_t mmu_nextFreeKernelPage();

void mmu_mapPage(uint32_t virtual, uint32_t cr3, uint32_t phy, uint8_t rw, uint8_t us);

uint32_t mmu_unmapPage(uint32_t virtual, uint32_t cr3);

uint32_t mmu_initKernelDir();

uint32_t game_calcularPos(uint32_t x, uint32_t y);

uint32_t mmu_initTaskDir(uint8_t player, uint8_t idx, uint32_t posx, uint32_t posy);

void mmu_mapNewPos(uint8_t player, uint32_t cr3, uint32_t posx, uint32_t posy);

void mmu_refreshPos(uint8_t player, uint32_t cr3, uint32_t new_pos);

uint32_t cr3s[20];

typedef struct pde_t {
    uint8_t p:1;
    uint8_t r_w:1;
    uint8_t u_s:1;
    uint8_t pwt:1;
    uint8_t pcd:1;
    uint8_t a:1;
    uint8_t zero:1;
    uint8_t ps: 1;
    uint8_t g:1;
    uint8_t avl:3;
    uint32_t dir:20;
} __attribute__((__packed__)) pde;

typedef struct pte_t {
    uint8_t p:1;
    uint8_t r_w:1;
    uint8_t u_s:1;
    uint8_t pwt:1;
    uint8_t pcd:1;
    uint8_t a:1;
    uint8_t d:1;
    uint8_t pat:1;
    uint8_t g:1;
    uint8_t avl:3;
    uint32_t dir:20;
} __attribute__((__packed__)) pte;

#endif  /* !__MMU_H__ */
