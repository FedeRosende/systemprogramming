/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del scheduler
*/

#ifndef __SCHED_H__
#define __SCHED_H__

#include "stdint.h"
#include "screen.h"
#include "tss.h"

/* Struct del shceduler */
typedef struct scheduler_t {
	//creo dos vectores  de estados con 10 entradas c/u, una para cada tarea de cada jugador
	uint8_t estadosA[10];
	uint8_t estadosB[10];
	uint8_t jugadorActual;					// 0-> tarea de B en ejecución, 1->tarea de A en ejecución
	uint16_t indiceTareaActualA; 			//indice dentro del vector de estadosB.
	uint16_t indiceTareaActualB; 			//indice dentro del vector de estadosA.
} __attribute__((__packed__, aligned (8))) sched;

void sched_init();

uint16_t sched_nextTask();

uint16_t player_nextTask(uint8_t player);

uint8_t neg(uint8_t player);

uint8_t current_id();

uint16_t kill_task();

uint8_t get_free_pos(uint8_t player);

sched* scheduler;

#endif	/* !__SCHED_H__ */
