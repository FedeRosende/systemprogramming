/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del manejador de memoria
*/

#include "mmu.h"

uint32_t next_free_page;

void mmu_init() {
  next_free_page = 0x100000;
}

uint32_t mmu_nextFreeKernelPage() {
  uint32_t page = next_free_page;
  next_free_page += 0x1000;
  return page;
}

void mmu_mapPage(uint32_t virtual, uint32_t cr3, uint32_t phy, uint8_t rw, uint8_t us) {

  pde *pd = (pde*) ((cr3 >> 12) <<12);
  uint32_t pd_idx = (virtual >> 22);
  uint32_t pt_idx = (virtual << 10 >> 22);

  if (pd[pd_idx].p != 1){ //si el índice no está presente lo inicializo
    pd[pd_idx].p = 1;
    pd[pd_idx].r_w = rw;
    pd[pd_idx].u_s = us;
    pd[pd_idx].pwt = 1;
    pd[pd_idx].pcd = 0;
    pd[pd_idx].a = 0;
    pd[pd_idx].zero = 0;
    pd[pd_idx].ps = 0;
    pd[pd_idx].g = 0;
    pd[pd_idx].avl = 0;
    pte *pt_dir = (pte*) mmu_nextFreeKernelPage();
    pd[pd_idx].dir = ((uint32_t) (pt_dir)) >> 12;

    for (uint32_t i = 0; i < 1024; i++) { //inicializo la nueva tabla
      pt_dir[i].p = 0;
    }

  }

  pte *pt = (pte*)(pd[pd_idx].dir << 12);
  pt[pt_idx].p = 1;
  pt[pt_idx].r_w = rw;
  pt[pt_idx].u_s = us;
  pt[pt_idx].pwt = 0;
  pt[pt_idx].pcd = 0;
  pt[pt_idx].a = 0;
  pt[pt_idx].d = 0;
  pt[pt_idx].pat = 0;
  pt[pt_idx].g = 0;
  pt[pt_idx].avl = 0;
  pt[pt_idx].dir = (phy >> 12);

  tlbflush();
}

uint32_t mmu_unmapPage(uint32_t virtual, uint32_t cr3) {
  pde *pd = (pde*) ((cr3 >> 12) <<12);
  uint32_t pd_idx = (virtual >> 22);
  uint32_t pt_idx = (virtual << 10 >> 22);
  if (pd[pd_idx].p) {
    pte *pt = (pte*) (pd[pd_idx].dir << 12);
    pt[pt_idx].p = 0;
    tlbflush();
  }
  return 0;
}

uint32_t mmu_initKernelDir() {

  pde *pd = (pde*) 0x00027000;
  pte *pt = (pte*) 0x0028000;

  pd[0].p = 1;
  pd[0].r_w = 1;
  pd[0].u_s = 0;
  pd[0].pwt = 0;
  pd[0].pcd = 0;
  pd[0].a = 0;
  pd[0].zero = 0;
  pd[0].ps = 0;
  pd[0].g = 0;
  pd[0].avl = 0;
  pd[0].dir = 0x0028;
  for (uint32_t i = 0; i < 1024; i++) {
    pt[i].p = 1;
    pt[i].r_w = 1;
    pt[i].u_s = 0;
    pt[i].pwt = 0;
    pt[i].pcd = 0;
    pt[i].a = 0;
    pt[i].d = 0;
    pt[i].pat = 0;
    pt[i].g = 0;
    pt[i].avl = 0;
    pt[i].dir = i;
  }
  for (uint32_t i = 1; i < 1024; i++) {
    pd[i].p = 0;
  }

  return 0;
}


uint32_t game_calcularPos(uint32_t x, uint32_t y){
  uint32_t pos = 0x400000 + x * 8192 + y * 78 * 8192;
  return pos;
}

uint32_t mmu_initTaskDir(uint8_t player, uint8_t idx, uint32_t posx, uint32_t posy){
  uint32_t new_cr3 = mmu_nextFreeKernelPage();
  //4mb con identity mapping
  for (uint32_t i = 0; i < 0x3fffff; i+=0x1000) {
    mmu_mapPage(i, new_cr3, i, 1, 0);
  }

  mmu_mapNewPos(player, new_cr3, posx, posy);

  cr3s[idx] = new_cr3;

  return new_cr3;
}

void mmu_mapNewPos(uint8_t player, uint32_t cr3, uint32_t posx, uint32_t posy){
  //player = 1 => A | player = 0 => B
  uint32_t task_code = player ? 0x10000 : 0x12000; //La tengo mapeada con identity mapping
  uint32_t shared = player ? 0x15000 : 0x17000;

  //Calculo la nueva posición
  uint32_t new_pos = game_calcularPos(posx, posy);

  //Copio el código de la tarea a la nueva posición

  mmu_mapPage(new_pos, rcr3(), new_pos, 1, 0); //La mapeo con identity mapping solamente para copiar el código
  mmu_mapPage(new_pos+0x1000, rcr3(), new_pos+0x1000, 1, 0);
  for (uint32_t i = 0; i < 0x2000; i+=4) { //Copio ambas páginas de código a la posición en el mapa
    uint32_t* pagina = (uint32_t *)(new_pos+i);
    uint32_t* codigo = (uint32_t*)(task_code+i);
    *pagina = *codigo;
  }
  mmu_unmapPage(new_pos, rcr3());
  mmu_unmapPage(new_pos+0x1000, rcr3());

  mmu_mapPage(0x08000000, cr3, new_pos, 1, 1); //Mapeo la primera página usando el directorio de la tarea
  mmu_mapPage(0x08001000, cr3, new_pos+0x1000, 1, 1); //Mapeo la segunda página usando el directorio de la tarea
  mmu_mapPage(0x08002000, cr3, shared, 1, 1); //Mapeo la primera página del área compartida usando el directorio de la tarea
  mmu_mapPage(0x08003000, cr3, shared+0x1000, 1, 1); //Mapeo la segunda página del área compartida usando el directorio de la tarea

  tlbflush();
}

void mmu_refreshPos(uint8_t player, uint32_t cr3, uint32_t new_pos){
  uint32_t shared = player ? 0x15000 : 0x17000;

  mmu_mapPage(new_pos, rcr3(), new_pos, 1, 0);
  mmu_mapPage(new_pos+0x1000, rcr3(), new_pos+0x1000, 1, 0);
  for (uint32_t i = 0; i < 0x2000; i+=4) {
    uint32_t* new_code = (uint32_t *)(new_pos+i);
    uint32_t* old_code = (uint32_t *)(0x08000000+i);
    *new_code = *old_code;
  }
  mmu_unmapPage(new_pos, rcr3());
  mmu_unmapPage(new_pos+0x1000, rcr3());

  mmu_mapPage(0x08000000, cr3, new_pos, 1, 1);
  mmu_mapPage(0x08001000, cr3, new_pos+0x1000, 1, 1);
  mmu_mapPage(0x08002000, cr3, shared, 1, 1);
  mmu_mapPage(0x08003000, cr3, shared+0x1000, 1, 1);

  tlbflush();
}
