/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
*/

#include "game.h"
#include "colors.h"


void tasks_init(){
	for (uint8_t i = 0; i < 20; i++) {
		tasks[i].posx = 0;
		tasks[i].posy = 0;
    tasks[i].cristales = 0;
	}
}

void game_init() {

  //Jugador A
  players[0].posy = 20;
  players[0].score = 0;

  //Jugador B
  players[1].posy = 20;
  players[1].score = 0;

  tasks_init();

  screen_init();

  map_init();

	for (int i = 1; i < 11; i++) {
		print((uint8_t *) "x\0", 3*i+1, 44, C_FG_RED | C_BG_BLACK);
		print((uint8_t *) "x\0", 3*i+1+46, 44, C_FG_RED | C_BG_BLACK);
	}
}

void screen_init(){
  screen_drawBox(0, 0, 40, 78, 0x0, 0x00);
  screen_drawBox(1, 1, 39, 78, 0x0, 0xFF);
  print_dec(0, 1, 0, 20, 0x04);
  print_dec(0, 1, 79, 20, 0x01);
}

void map_init(){
	uint8_t textPlayer[10];
  textPlayer[0] = 74;  //J
  textPlayer[1] = 117; //u
  textPlayer[2] = 103; //g
  textPlayer[3] = 97;  //a
  textPlayer[4] = 100; //d
  textPlayer[5] = 111; //o
  textPlayer[6] = 114; //r
  textPlayer[7] = 32;  //(whitespace)
  textPlayer[8] = 65;  //A
  textPlayer[9] =  0;
	print(textPlayer, 1, 42, C_FG_WHITE | C_BG_BLACK);
	textPlayer[8] = 66;  //B
	print(textPlayer, 70, 42, C_FG_WHITE | C_BG_BLACK);

	screen_drawBox(43, 35, 3, 5, 000, C_BG_RED); //box vidas izquierda
	screen_drawBox(43, 40, 3, 5, 000, C_BG_BLUE); //box vidas izquierda

	uint8_t player_tasks[30];
	player_tasks[0] = (uint8_t) '0';
	player_tasks[1] = 32;
	player_tasks[2] = 32;
	player_tasks[3] = (uint8_t) '1';
	player_tasks[4] = 32;
	player_tasks[5] = 32;
	player_tasks[6] = (uint8_t) '2';
	player_tasks[7] = 32;
	player_tasks[8] = 32;
	player_tasks[9] = (uint8_t) '3';
	player_tasks[10] = 32;
	player_tasks[11] = 32;
	player_tasks[12] = (uint8_t) '4';
	player_tasks[13] = 32;
	player_tasks[14] = 32;
	player_tasks[15] = (uint8_t) '5';
	player_tasks[16] = 32;
	player_tasks[17] = 32;
	player_tasks[18] = (uint8_t) '6';
	player_tasks[19] = 32;
	player_tasks[20] = 32;
	player_tasks[21] = (uint8_t) '7';
	player_tasks[22] = 32;
	player_tasks[23] = 32;
	player_tasks[24] = (uint8_t) '8';
	player_tasks[25] = 32;
	player_tasks[26] = 32;
	player_tasks[27] = (uint8_t) '9';
	player_tasks[28] = 0;

	print(player_tasks, 3, 44, C_FG_WHITE | C_BG_BLACK);
	print(player_tasks, 49, 44, C_FG_WHITE | C_BG_BLACK);


	uint8_t score[4];
	for (int i = 0; i < 4; i++) {
		score[i] = (uint8_t)'0';
	}
	score[3] = 0;

	print(score, 36, 44, C_FG_WHITE | C_BG_RED);
	print(score, 41, 44, C_FG_WHITE | C_BG_BLUE);

	for (uint8_t i = 0; i < 29; i+=3) {
		player_tasks[i] = (uint8_t) '0';
		player_tasks[i+1] = (uint8_t) '0';
	}
	player_tasks[29] = 0;
	print(player_tasks, 3, 45, C_FG_LIGHT_GREY | C_BG_BLACK);
	print(player_tasks, 49, 45, C_FG_LIGHT_GREY | C_BG_BLACK);

	for (uint16_t i = 20; i < 60; i++) {
		for (uint16_t j = 0; j < 40; j++) {
			map[i][j] = 9;
		}
	}

	for (uint16_t i = 1; i < 79; i++) {
	  for (uint16_t j = 0; j < 41; j++) {
			print_dec(map[i][j], 1, i, j, C_FG_BLUE | C_BG_CYAN);
	  }
	}
}

void is_finished() {
	if(players[0].score >= 300){
		screen_drawBox(10, 20, 20, 40, 0, C_BG_RED);
		char * msj = "Ganador jugador A!\0";
		print((uint8_t *) msj, 30, 18, C_BG_RED | C_FG_WHITE);
		msj = "Fin del juego.\0";
		print((uint8_t *) msj, 33, 20, C_BG_RED | C_FG_WHITE);
		hlt();
	} else if(players[1].score >= 300){
		screen_drawBox(10, 20, 20, 40, 0, C_BG_BLUE);
		char * msj = "Ganador jugador B!\0";
		print((uint8_t *) msj, 30, 18, C_BG_BLUE | C_FG_WHITE);
		msj = "Fin del juego.\0";
		print((uint8_t *) msj, 33, 20, C_BG_BLUE | C_FG_WHITE);
		hlt();
	}
}
