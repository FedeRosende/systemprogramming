; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================

%include "print.mac"



global start
extern GDT_DESC
extern IDT_DESC
extern idt_init
extern pic_reset
extern pic_enable
extern mmu_init
extern mmu_initKernelDir
extern tss_init
extern sched_init
extern game_init


;; Saltear seccion de datos
jmp start

;;
;; Seccion de datos.
;; -------------------------------------------------------------------------- ;;
start_rm_msg db     'Iniciando kernel en Modo Real'
start_rm_len equ    $ - start_rm_msg

start_pm_msg db     'Iniciando kernel en Modo Protegido'
start_pm_len equ    $ - start_pm_msg

;;
;; Seccion de código.
;; -------------------------------------------------------------------------- ;;

;; Punto de entrada del kernel.
BITS 16
start:
    ; Deshabilitar interrupciones
    cli

    ; Cambiar modo de video a 80 X 50
    mov ax, 0003h
    int 10h ; set mode 03h
    xor bx, bx
    mov ax, 1112h
    int 10h ; load 8x8 font

    ; Imprimir mensaje de bienvenida
    print_text_rm start_rm_msg, start_rm_len, 0x07, 0, 0


    ; Habilitar A20

    call A20_enable

    ; Cargar la GDT

    lgdt [GDT_DESC]

    ; Setear el bit PE del registro CR0

    mov eax, cr0
    or eax, 1
    mov cr0, eax

    ; Saltar a modo protegido

    jmp 0x98:modoProtegido

BITS 32
modoProtegido:

    ; Establecer selectores de segmentos

    mov ax, 0x90
    mov ss, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    ; Establecer la base de la pila

    mov esp, 0x27000
    mov ebp, 0x27000

    ; Imprimir mensaje de bienvenida

    ;mov ax, 0xB0
    ;mov fs, ax

    ;xor eax, eax

; Inicializar pantalla

    call game_init

;icloPantalla:
;    cmp eax, 0x1F3F
;    jge sigo
;    mov word[fs:eax], 0xD100
;    add eax, 2
;    jmp cicloPantalla

;sigo:

    ;mov ax, ds
    ;mov fs, ax

    ; Inicializar el manejador de memoria

    call mmu_init

    ; Inicializar el directorio de paginas

    call mmu_initKernelDir

    ; Cargar directorio de paginas

    mov eax, 0x00027000
    mov cr3, eax

    ; Habilitar paginacion

    mov eax, cr0
    or eax, 0x80000000
    mov cr0, eax

    ; Inicializar tss
    ; Inicializar tss de la tarea Idle

    call tss_init ;esta función inicializa todo

    ; Inicializar el scheduler

    call sched_init

    ; Inicializar la IDT

    call idt_init

    ; Cargar IDT

    lidt[IDT_DESC]

    ; Configurar controlador de interrupciones

    call pic_reset
    call pic_enable

    ; Cargar tarea inicial

    mov ax, 0xB8
    ltr ax

    ; Habilitar interrupciones

    sti

    ; Saltar a la primera tarea: Idle

    jmp 0xC0:0

    ; Ciclar infinitamente (por si algo sale mal...)
    mov eax, 0xFFFF
    mov ebx, 0xFFFF
    mov ecx, 0xFFFF
    mov edx, 0xFFFF
    jmp $

;; -------------------------------------------------------------------------- ;;




%include "a20.asm"
