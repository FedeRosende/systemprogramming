; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================
; definicion de rutinas de atencion de interrupciones

%include "print.mac"

BITS 32

offset: dd 0
selector: dw 0

flag: dd 0

;Mensajes de error
msg_error_int0: db "Divide Error"
lng_int0 : equ $-msg_error_int0
msg_error_int1: db "Reservada"
lng_int1 : equ $-msg_error_int1
msg_error_int2: db "NMI"
lng_int2 : equ $-msg_error_int2
msg_error_int3: db "Breakpoint"
lng_int3 : equ $-msg_error_int3
msg_error_int4: db "Overflow"
lng_int4 : equ $-msg_error_int4
msg_error_int5: db "BOUND Range Exceeded"
lng_int5 : equ $-msg_error_int5
msg_error_int6: db "Invalid Opcode (Undefined Opcode)"
lng_int6 : equ $-msg_error_int6
msg_error_int7: db "Device Not Available (No Math Coprocessor)"
lng_int7 : equ $-msg_error_int7
msg_error_int8: db "Double Fault"
lng_int8 : equ $-msg_error_int8
msg_error_int9: db "Coprocessor Segment Overrun"
lng_int9 : equ $-msg_error_int9
msg_error_int10: db "Invalid TSS"
lng_int10 : equ $-msg_error_int10
msg_error_int11: db "Segment Not Present"
lng_int11 : equ $-msg_error_int11
msg_error_int12: db "Stack-Segment Fault"
lng_int12 : equ $-msg_error_int12
msg_error_int13: db "General Protection"
lng_int13 : equ $-msg_error_int13
msg_error_int14: db "Page Fault"
lng_int14 : equ $-msg_error_int14
msg_error_int15: db "Reserved"
lng_int15 : equ $-msg_error_int15
msg_error_int16: db "FPU Floating-Point Error (Math Fault)"
lng_int16 : equ $-msg_error_int16
msg_error_int17: db "Alignment Check"
lng_int17 : equ $-msg_error_int17
msg_error_int18: db "Machine Check"
lng_int18 : equ $-msg_error_int18
msg_error_int19: db "SIMD Floating-Point Exception"
lng_int19 : equ $-msg_error_int19

;Numeros por scan
num_code_1: db "1"
lng_1: equ $-num_code_1

num_code_2: db "2"
lng_2: equ $-num_code_2

num_code_3: db "3"
lng_3: equ $-num_code_3

num_code_4: db "4"
lng_4: equ $-num_code_4

num_code_5: db "5"
lng_5: equ $-num_code_5

num_code_6: db "6"
lng_6: equ $-num_code_6

num_code_7: db "7"
lng_7: equ $-num_code_7

num_code_8: db "8"
lng_8: equ $-num_code_8

num_code_9: db "9"
lng_9: equ $-num_code_9

num_code_0: db "0"
lng_0: equ $-num_code_0


clean: db " "
lng_clean: equ $-clean




;; PIC
extern pic_finish1
extern pic_enable
extern deshabilitar_reloj

;; Sched
extern sched_nextTask
extern kill_task
extern crear_tarea_A
extern crear_tarea_B
extern subo_A
extern bajo_A
extern subo_B
extern bajo_B
extern current_id
extern move_task
extern task_take
extern is_finished

;Modo debug

extern print_debug_mode
extern print_normal_mode
extern print_procesador
extern print_map_task

;;
;; Definición de MACROS
;; -------------------------------------------------------------------------- ;;
section .text

%macro ISR 1
global _isr%1

_isr%1:
    ;Usar para modo debug
    add esp, 4 ;ignoro el error code
    pushad
    mov edi, esp ;guardo la dirección de la pila para modo debug

    call kill_task
    mov ecx, [flag]
    cmp ecx, 0
    je .task_switch

    ;modo debug activado
    call deshabilitar_reloj
    push edi
    call print_procesador
    pop edi
    mov eax, %1
    print_text_pm msg_error_int%1, lng_int%1, 0x0000007F, 3, 22
    jmp 0xC0:0 ;salto a la idle

.task_switch:
    mov [selector], ax ;Salto a la próxima o a la idle
    jmp far [offset]
%endmacro

%macro NUM_CODE 1
global _num_code%1
_num_code%1:
    pushad
    mov eax, %1
    print_text_pm num_code_%1, lng_%1, 0x0000000F, 0, 0
    popad
%endmacro



;; Rutina de atención de las EXCEPCIONES
;; -------------------------------------------------------------------------- ;;

ISR 0
ISR 1
ISR 2
ISR 3
ISR 4
ISR 5
ISR 6
ISR 7
ISR 8
ISR 9
ISR 10
ISR 11
ISR 12
ISR 13
ISR 14
ISR 15
ISR 16
ISR 17
ISR 18
ISR 19

;; Rutina de atención del RELOJ
;; -------------------------------------------------------------------------- ;;
global _isr32
_isr32:
  pushad
  call pic_finish1
  call is_finished
  call sched_nextTask
  call nextClock
  str cx
  cmp ax, cx
  je .fin
    mov [selector], ax
    jmp far [offset]
  .fin:
  popad
  iret

;; Rutina de atención del TECLADO
;; -------------------------------------------------------------------------- ;;
global _isr33
_isr33:
  pushad
  call pic_finish1
  in al, 0x60
  cmp al, 0x2A
  jne .suboA
  call crear_tarea_A
  cmp al, 0x11
  jne .fin
.suboA:
  cmp al, 0x11
  jne .bajoA
  call subo_A
.bajoA:
  cmp al, 0x1F
  jne .suboB
  call bajo_A
.suboB:
  cmp al, 0x17
  jne .bajoB
  call subo_B
.bajoB:
  cmp al, 0x25
  jne .creotareaB
  call bajo_B
.creotareaB:
  cmp al, 0x36
  jne .modoDebug
  call crear_tarea_B
.modoDebug:
  cmp al, 0x15
  jne .fin
  ;not de flag debug global
  mov ecx, [flag]
  not ecx
  mov [flag], ecx
  cmp ecx, 0
  je .modoNormal
  ;si variable global seteada
    ;print modo debug en pantalla
    call print_debug_mode
    jmp .fin
  ;si variable global no seteada
.modoNormal:
  ;habilito  interrupción de reloj (deshabilitada en la rai)
  call pic_enable
  call print_normal_mode
  ;print de map y las tareas activas
  call print_map_task
.fin:
  popad
  iret


;; Rutinas de atención de las SYSCALLS
;; -------------------------------------------------------------------------- ;;
global _isr47
_isr47:
    pushf
    push edi
    push esi
    push edx
    push ecx
    push ebx
    cmp eax, 760279
    jne .move
    call current_id
    jmp .fin
.move:
    cmp eax, 177788
    jne .take
    push ebx
    call move_task
    pop ebx
    jmp 0xC0:0
    jmp .fin
.take:
    push ebx
    call task_take
    pop ebx
    jmp 0xC0:0
.fin:
    pop ebx
    pop ecx
    pop edx
    pop esi
    pop edi
    popf
    iret

;; Funciones Auxiliares
;; -------------------------------------------------------------------------- ;;
isrNumber:             dd 0x00000000
isrNumber0A:           dd 0x00000000
isrNumber1A:           dd 0x00000000
isrNumber2A:           dd 0x00000000
isrNumber3A:           dd 0x00000000
isrNumber4A:           dd 0x00000000
isrNumber5A:           dd 0x00000000
isrNumber6A:           dd 0x00000000
isrNumber7A:           dd 0x00000000
isrNumber8A:           dd 0x00000000
isrNumber9A:           dd 0x00000000
isrNumber0B:           dd 0x00000000
isrNumber1B:           dd 0x00000000
isrNumber2B:           dd 0x00000000
isrNumber3B:           dd 0x00000000
isrNumber4B:           dd 0x00000000
isrNumber5B:           dd 0x00000000
isrNumber6B:           dd 0x00000000
isrNumber7B:           dd 0x00000000
isrNumber8B:           dd 0x00000000
isrNumber9B:           dd 0x00000000
isrClock:              db '|/-\'
nextClock:
        pushad
        shr eax, 3
        cmp eax, 25
        jne .1A
        inc DWORD [isrNumber0A]
        mov ebx, [isrNumber0A]
        cmp ebx, 0x4
        mov edx, 4
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber0A], 0
        jmp .continue

.1A:
        cmp eax, 26
        jne .2A
        inc DWORD [isrNumber1A]
        mov ebx, [isrNumber1A]
        cmp ebx, 0x4
        mov edx, 7
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber1A], 0
        jmp .continue

.2A:
        cmp eax, 27
        jne .3A
        inc DWORD [isrNumber2A]
        mov ebx, [isrNumber2A]
        cmp ebx, 0x4
        mov edx, 10
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber2A], 0
        jmp .continue

.3A:
        cmp eax, 28
        jne .4A
        inc DWORD [isrNumber3A]
        mov ebx, [isrNumber3A]
        cmp ebx, 0x4
        mov edx, 13
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber3A], 0
        jmp .continue

.4A:
        cmp eax, 29
        jne .5A
        inc DWORD [isrNumber4A]
        mov ebx, [isrNumber4A]
        cmp ebx, 0x4
        mov edx, 16
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber4A], 0
        jmp .continue

.5A:
        cmp eax, 30
        jne .6A
        inc DWORD [isrNumber5A]
        mov ebx, [isrNumber5A]
        cmp ebx, 0x4
        mov edx, 19
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber5A], 0
        jmp .continue

.6A:
        cmp eax, 31
        jne .7A
        inc DWORD [isrNumber6A]
        mov ebx, [isrNumber6A]
        cmp ebx, 0x4
        mov edx, 22
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber6A], 0
        jmp .continue

.7A:
        cmp eax, 32
        jne .8A
        inc DWORD [isrNumber7A]
        mov ebx, [isrNumber7A]
        cmp ebx, 0x4
        mov edx, 25
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber7A], 0
        jmp .continue

.8A:
        cmp eax, 33
        jne .9A
        inc DWORD [isrNumber8A]
        mov ebx, [isrNumber8A]
        cmp ebx, 0x4
        mov edx, 28
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber8A], 0
        jmp .continue

.9A:
        cmp eax, 34
        jne .0B
        inc DWORD [isrNumber9A]
        mov ebx, [isrNumber9A]
        cmp ebx, 0x4
        mov edx, 31
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber9A], 0
        jmp .continue

.0B:
        cmp eax, 35
        jne .1B
        inc DWORD [isrNumber0B]
        mov ebx, [isrNumber0B]
        cmp ebx, 0x4
        mov edx, 50
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber0B], 0
        jmp .continue

.1B:
        cmp eax, 36
        jne .2B
        inc DWORD [isrNumber1B]
        mov ebx, [isrNumber1B]
        cmp ebx, 0x4
        mov edx, 53
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber1B], 0
        jmp .continue

.2B:
        cmp eax, 37
        jne .3B
        inc DWORD [isrNumber2B]
        mov ebx, [isrNumber2B]
        cmp ebx, 0x4
        mov edx, 56
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber2B], 0
        jmp .continue


.3B:
        cmp eax, 38
        jne .4B
        inc DWORD [isrNumber3B]
        mov ebx, [isrNumber3B]
        cmp ebx, 0x4
        mov edx, 59
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber3B], 0
        jmp .continue

.4B:
        cmp eax, 39
        jne .5B
        inc DWORD [isrNumber4B]
        mov ebx, [isrNumber4B]
        cmp ebx, 0x4
        mov edx, 62
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber4B], 0
        jmp .continue

.5B:
        cmp eax, 40
        jne .6B
        inc DWORD [isrNumber5B]
        mov ebx, [isrNumber5B]
        cmp ebx, 0x4
        mov edx, 65
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber5B], 0
        jmp .continue

.6B:
        cmp eax, 41
        jne .7B
        inc DWORD [isrNumber6B]
        mov ebx, [isrNumber6B]
        cmp ebx, 0x4
        mov edx, 68
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber6B], 0
        jmp .continue

.7B:
        cmp eax, 42
        jne .8B
        inc DWORD [isrNumber7B]
        mov ebx, [isrNumber7B]
        cmp ebx, 0x4
        mov edx, 71
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber7B], 0
        jmp .continue

.8B:
        cmp eax, 43
        jne .9B
        inc DWORD [isrNumber8B]
        mov ebx, [isrNumber8B]
        cmp ebx, 0x4
        mov edx, 74
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber8B], 0
        jmp .continue

.9B:
        cmp eax, 44
        jne .fin
        inc DWORD [isrNumber9B]
        mov ebx, [isrNumber9B]
        cmp ebx, 0x4
        mov edx, 77
        jl .continue
        mov ebx, 0
        mov DWORD [isrNumber9B], 0
        jmp .continue


.continue:
        add ebx, isrClock
        print_text_pm ebx, 1, 0x0f, 44, edx

.fin:
        inc DWORD [isrNumber]
        mov ebx, [isrNumber]
        cmp ebx, 0x4
        jl .ok
                mov DWORD [isrNumber], 0x0
                mov ebx, 0
        .ok:
                add ebx, isrClock
                print_text_pm ebx, 1, 0x0f, 49, 79
                popad
        ret
